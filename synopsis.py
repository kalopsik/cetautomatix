
# ++ κωδικός είδους
# ++ αναλώσιμα
#

import sys
import openpyxl

ctables_file = sys.argv[1]
synopsis_file = sys.argv[2]

#wb = openpyxl.load_workbook("ctables18.xlsx", read_only=True, data_only=True)
CTables = openpyxl.load_workbook(ctables_file, data_only=True)
WorkSheets = CTables.sheetnames
#print(Sheets)

#ws = wb['ComputerParts']
#ws.calculate_dimension()
#Cols = ws['A:G']
#type(Cols[2])
#L = [len(Cols[i]) for i in range(len(Cols))]
#print(L)
#print(type(ws['P'][22].value))
#WorkSheets = wb.sheetnames
Depts = CTables[WorkSheets[-1]]
Synopsis = openpyxl.Workbook()

# True if we are within Κατανομή, False otherwise
in_distr = lambda ws,i: ws['P'][i].value == 1 and ws['B'][i].value.strip() != 'START'

for sheet in WorkSheets:
    print("============")
    print(sheet)
    print("------------")
    ws = CTables[sheet]
    for i in range(len(ws['P'])):
        
        dept = ws['c'][i].value
        
        if in_distr(ws,i) and  dept != None:

            equipment_id = '{}.{}'.format(ws['k'][i].value.strip(), ws['l'][i].value+1)
            equipment = ws['B'][i].value  # material
            quantity = ws['d'][i].value  # quantity
            cost_per_unit = ws['g'][i].value  # cost per unit
            cost = ws['h'][i].value  # cost
            KAE = ws['f'][i].value  # KAE
            dept = dept.strip()
            
            if dept not in Synopsis.sheetnames:
                synopsis_ws = Synopsis.create_sheet(dept)
                row = ('ID','Εξοπλισμός', 'Τμήμα', 'Ποσότητα', 'Κόστος/Τεμάχιο (ευρώ)', 'Κόστος', 'ΚΑΕ')
                synopsis_ws.append(row)
                print('New sheet created: {}'.format(Synopsis.sheetnames))
            else:
                synopsis_ws = Synopsis[dept]
                
            row = (equipment_id,equipment, dept, quantity, cost_per_unit, cost, KAE)
#             for t in row:
#                 print(type(t))
#             print(row)
            
            synopsis_ws.append(row)

Synopsis.save(synopsis_file)
