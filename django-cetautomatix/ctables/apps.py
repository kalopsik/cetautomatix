from django.apps import AppConfig


class CtablesConfig(AppConfig):
    name = 'ctables'
