from django.db import models

# We need functionality: copy, paste, delete (actually mark as deleted), duplicate
# Whatever starts with R_ is a relation

class CTable(models.Model):

    title_text = models.CharField(max_length=200)
    # we want to keep all version of a ctable
    version = 0
    created_date = models.DateTimeField('Date created')
    estimated_cost = models.FloatField()
    comments_text = models.TextField()


# One CTable <--> Many Attachment
class R_CTable_Attachment(models.Model):
    ctable = models.ForeignKey(CTable, on_delete=models.CASCADE)
    attachment = models.ForeignKey(Attachment, on_delete=models.CASCADE)

# Relation between ctables and Departments
# list of pairs departments and ctables; which ctable
# belongs to which department.
#
# Many Ctables <--> Many Departments
class R_CTable_Department(models.Model):
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    ctable = models.ForeignKey(CTable, on_delete=models.CASCADE)

# Many CTable <--> One KAE
class R_CTable_KAE(models.Model):
    ctable = models.ForeignKey(CTable, on_delete=models.CASCADE)
    kae = models.ForeignKey(KAE, on_delete=models.CASCADE)

# Many CTable <--> One Group
class R_CTable_Group(models.Model):
    ctable = models.ForeignKey(CTable, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)

# Many CTable <--> One Category
class R_CTable_Category(models.Model):
    ctable = models.ForeignKey(CTable, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

class Attachment(models.Model):
    filename_text = models.CharField(max_length=100)

######################################
# The groups of the procurement
class Group(model.Models):
    group_title_text = models.CharField(max_length=200)
    group_budget = 0

# Equipment categories (e.g. Computer, Printer, Monitor, etc)
class Category(models.Model):

    category_text = models.CharField(max_length=200)

class KAE(models.Model):
    code_text = models.CharField(max_length=10)
    description_text = models.CharField(max_length=200)

# The departments that everntually get the specified equipment
#
# Notes:
#
#    - We need budget per KAE (sum of estimations of costs)
#    - Budget must not exceed budget in the logistirio
class Department(models.Model):
    dept_title_text = models.CharField(max_length=200)

class Specs(models.Model):
    ctable = models.ForeignKey(CTable, on_delete=models.CASCADE)
    description_text = models.CharField(max_length=500)
    requirement_text = models.CharField(max_length=500)

class CTableCost(model.Models):
    cost_float = models.FloatField()

class Attachment(model.Models):
    description_text = models.CharField(max_length=100)